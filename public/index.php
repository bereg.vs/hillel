<?php

use Nemo\Hillel\Router;

require (__DIR__ . "/../vendor/autoload.php");

$container = new DI\ContainerBuilder();
$container->addDefinitions(__DIR__ . "/../config/di/params.php");
$container->addDefinitions(__DIR__ . "/../config/di/services.php");
$container = $container->build();

$router = new Router($_SERVER['REQUEST_URI']);

try {
    $controller = $container->get($router->getController());
    $method = $router->getMethod();
    call_user_func_array([$controller, $method], $router->getArguments());
} catch (\Throwable $e) {
    echo $e->getMessage();
}
