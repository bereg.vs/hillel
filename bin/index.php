<?php

use Nemo\Hillel\Calc\Figures\{Triangle, Rectangle, Square, Circle};
use Nemo\Hillel\Calc\Calc;

require (__DIR__ . "/../vendor/autoload.php");

    $calc = new Calc([
        Rectangle::class,
        Square::class,
        Triangle::class,
        Circle::class
    ]);

    $figures = $calc->getFigures();

    echo 'Вкажiть фiгуру для обчислення ' . PHP_EOL;
    foreach($figures as $key => $figure) {
        echo $key . ' - ' . $figure::getFigureName() . PHP_EOL;
    }

    do {
        $inputType = readline('Введите число' . PHP_EOL);
    } while (!in_array($inputType, array_keys($figures)));

    $calc->run($inputType);
