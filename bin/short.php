<?php

use Nemo\Hillel\LinkShortener\Interfaces\ICodeRepository;
use Nemo\Hillel\LinkShortener\UrlCodeConverter;

require (__DIR__ . "/../vendor/autoload.php");

$container = new DI\ContainerBuilder();
$container->addDefinitions(__DIR__ . "/../config/di/params.php");
$container->addDefinitions(__DIR__ . "/../config/di/services.php");
$container = $container->build();

$app = $container->get(UrlCodeConverter::class);

echo 'Вкажiть режим роботи ' . PHP_EOL;


do {
    $mode = readline('1 - Кодування URL, 2 Декодування URL ' . PHP_EOL);
} while (!in_array($mode, [1, 2]));

if ($mode === "1") {
    try {
        $url = readline('Введiть URL:' . PHP_EOL);
        echo $app->encode($url) . PHP_EOL;
    } catch (InvalidArgumentException $e) {
        echo $e->getMessage() . PHP_EOL;
    }
} else {
    try {
        $code = readline('Введiть Код:' . PHP_EOL);
        echo $app->decode($code) . PHP_EOL;
    } catch (InvalidArgumentException $e) {
        echo $e->getMessage() . PHP_EOL;
    }
}
