<?php

namespace Nemo\Hillel\LinkShortener\Interfaces;

interface IUrlCodeObject
{
    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return array
     */
    public function getAdditionalData(): array;
}