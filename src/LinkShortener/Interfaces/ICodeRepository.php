<?php

namespace Nemo\Hillel\LinkShortener\Interfaces;

use Nemo\Hillel\LinkShortener\Exceptions\EntityNotFoundException;
use Nemo\Hillel\LinkShortener\ValueObjects\UrlCode;

interface ICodeRepository
{
    /**
     * @param string $code
     * @throws EntityNotFoundException
     * @return IUrlCodeObject
     */
    public function getByCode(string $code): IUrlCodeObject;

    /**
     * @param string $code
     * @throws EntityNotFoundException
     * @return IUrlCodeObject
     */
    public function getByUrl(string $code): IUrlCodeObject;

    /**
     * @param string $code
     * @return bool
     */
    public function isCodeIsset(string $code): bool;

    /**
     * @param string $code
     * @param string $url
     * @return IUrlCodeObject
     */
    public function insertUrl(string $code, string $url): IUrlCodeObject;
}