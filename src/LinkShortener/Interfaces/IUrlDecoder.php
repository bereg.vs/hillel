<?php

namespace Nemo\Hillel\LinkShortener\Interfaces;

interface IUrlDecoder
{
    /**
     * @param string $code
     * @throws \InvalidArgumentException
     * @return string
     */
    public function decode(string $code): string;
}