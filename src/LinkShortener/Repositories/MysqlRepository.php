<?php

namespace Nemo\Hillel\LinkShortener\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Nemo\Hillel\LinkShortener\Exceptions\EntityNotFoundException;
use Nemo\Hillel\LinkShortener\Interfaces\ICodeRepository;
use Nemo\Hillel\LinkShortener\Interfaces\IUrlCodeObject;
use Nemo\Hillel\LinkShortener\ValueObjects\UrlCode;

class MysqlRepository implements ICodeRepository
{
    /**
     * @param string $model
     */
    public function __construct(protected string $model)
    {
    }

    /**
     * @inheritDoc
     */
    public function getByCode(string $code): IUrlCodeObject
    {
        try {
            $row = $this->model::where('code', $code)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new EntityNotFoundException('No code found');
        }

        return new UrlCode($row->code, $row->url, []);
    }

    /**
     * @inheritDoc
     */
    public function getByUrl(string $code): IUrlCodeObject
    {

        try {
            $row = $this->model::where('url', $code)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new EntityNotFoundException('No code found');
        }

        return new UrlCode($row->code, $row->url, []);
    }

    /**
     * @inheritDoc
     */
    public function isCodeIsset(string $code): bool
    {
        return $this->model::where('code', $code)->exists();
    }

    /**
     * @inheritDoc
     */
    public function insertUrl(string $code, string $url): IUrlCodeObject
    {
        $model = $this->model::create(['code' => $code, 'url' => $url]);

        return new UrlCode($model->code, $model->url, []);
    }
}