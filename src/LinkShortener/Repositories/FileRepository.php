<?php

namespace Nemo\Hillel\LinkShortener\Repositories;

use Nemo\Hillel\LinkShortener\Exceptions\{EntityNotFoundException, HandlingErrorException};
use Nemo\Hillel\LinkShortener\Interfaces\{ICodeRepository, IUrlCodeObject};
use Nemo\Hillel\LinkShortener\ValueObjects\UrlCode;
use SleekDB\Exceptions\{InvalidArgumentException, InvalidConfigurationException, IOException};
use SleekDB\Store;

class FileRepository implements ICodeRepository
{
    protected mixed $db = null;

    /**
     * @throws InvalidConfigurationException
     * @throws IOException
     * @throws InvalidArgumentException
     */
    public function __construct(protected string $dbFiles)
    {
        $this->db = new Store("urlCodes", $dbFiles, ['timeout' => false]);
    }

    public function getByCode(string $code): IUrlCodeObject
    {
        $row = $this->db->findOneBy(['code', '=', $code]);

        if ($row === null) {
            throw new EntityNotFoundException('Code not found');
        }

        return new UrlCode(
            $row['code'],
            $row['url'],
            $row['additionalData'],
        );
    }

    public function getByUrl(string $code): IUrlCodeObject
    {
        $row = $this->db->findOneBy(['url', '=', $code]);

        if ($row === null) {
            throw new EntityNotFoundException('Url not found');
        }

        return new UrlCode(
            $row['code'],
            $row['url'],
            $row['additionalData'],
        );
    }

    /**
     * @inheritDoc
     */
    public function isCodeIsset(string $code): bool
    {
        return is_null($this->db->findOneBy(['code', '=', $code]));
    }

    /**
     * @inheritDoc
     * @throws \InvalidArgumentException
     */
    public function insertUrl(string $code, string $url): IUrlCodeObject
    {
        try {
            $dbRow = $this->db->insert([
                'code' => $code,
                'url' => $url,
                'additionalData' => []
                ]);
            return new UrlCode($code, $url);
        } catch (\Exception $e) {
            throw new \InvalidArgumentException($e);
        }
    }

}