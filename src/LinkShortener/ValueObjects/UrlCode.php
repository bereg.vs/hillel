<?php

namespace Nemo\Hillel\LinkShortener\ValueObjects;

use Nemo\Hillel\LinkShortener\Interfaces\IUrlCodeObject;

class UrlCode implements IUrlCodeObject
{
    /**
     * @param string $code
     * @param string $url
     * @param array $additionalData
     */
    public function __construct(protected string $code,
                                protected string $url,
                                protected array  $additionalData = [])
    {
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return array
     */
    public function getAdditionalData(): array
    {
        return $this->additionalData;
    }
}