<?php

namespace Nemo\Hillel\LinkShortener\Exceptions;

use InvalidArgumentException;

class EntityNotFoundException extends InvalidArgumentException
{
}