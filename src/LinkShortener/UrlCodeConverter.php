<?php

namespace Nemo\Hillel\LinkShortener;

use Nemo\Hillel\LinkShortener\Interfaces\{ICodeRepository,
    IUrlDecoder,
    IUrlEncoder,
    IUrlValidator
};
use InvalidArgumentException;
use Nemo\Hillel\LinkShortener\Exceptions\EntityNotFoundException;
use Psr\Log\LoggerInterface;


class UrlCodeConverter implements IUrlDecoder, IUrlEncoder
{
    public function __construct(
        protected IUrlValidator $validator,
        protected ICodeRepository $repository,
        protected LoggerInterface $logger
    )
    {
    }

    /**
     * @inheritDoc
     */
    public function decode(string $code): string
    {
        try {
            $row = $this->repository->getByCode($code);
        } catch(EntityNotFoundException) {
            $this->logger->warning('Code not found', ['code' => $code]);
            throw new InvalidArgumentException("No code found");
        }

        return $row->getUrl();
    }

    /**
     * @param string $url
     * @throws InvalidArgumentException;
     * @return string
     */
    public function encode(string $url): string
    {
        $this->validator->validateUrl($url);

        $code = $this->generateCode($url);

        $row = $this->repository->insertUrl($code, $url);

        return $row->getCode();
    }

    public function generateCode(string $url): string
    {

        $crc = crc32($url);
        $hex = dechex($crc);

        $this->logger->info('URL: ' . $url . ' encoded to ' . $hex . PHP_EOL);

        return $hex;
    }
}