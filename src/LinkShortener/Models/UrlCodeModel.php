<?php

namespace Nemo\Hillel\LinkShortener\Models;

use Illuminate\Database\Eloquent\Model;

class UrlCodeModel extends Model
{
    protected $table = 'url_codes';

    public $timestamps = false;

    protected $fillable = ['code', 'url'];

}