<?php

namespace Nemo\Hillel\LinkShortener;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use Nemo\Hillel\LinkShortener\Interfaces\IUrlValidator;

class UrlValidator implements IUrlValidator
{
    protected ClientInterface $httpClient;

    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function isValidUrl(string $url): bool
    {
        if (empty($url) || !filter_var($url, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException('Invalid URL');
        }

        return true;
    }

    public function isRealUrl(string $url): bool
    {
        try {
            $this->httpClient->request('GET', $url);
        } catch (GuzzleException $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode());
        }

        return true;
    }

    public function validateUrl(string $url): bool
    {
        $this->isValidUrl($url);
        $this->isRealUrl($url);

        return true;
    }
}