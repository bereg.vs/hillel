<?php

namespace Nemo\Hillel\Calc\Figures;

class Square extends Polygon
{
    protected static string $figureName = "Квадрат";

    protected int $amountOfParameters = 1;

    public function getSquare(): int|float
    {
        return $this->shapes[0] ** 2;
    }

    public function getPerimeter(): int|float
    {
        return $this->shapes[0] * 4;
    }
}