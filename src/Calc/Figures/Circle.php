<?php

namespace Nemo\Hillel\Calc\Figures;

class Circle extends Figure
{
    protected static string $figureName = "Круг";

    public function getSquare(): float
    {
        return round(pi()*pow($this->shapes[0], 2), 2);

    }

    public function getPerimeter(): int|float
    {
        return round(2*pi()*$this->shapes[0], 2);
    }

    public function askShapes(): void
{
        do {
            $this->shapes[0] = readline('Укажите радиус' . PHP_EOL);
        } while (!is_numeric($this->shapes[0]) || $this->shapes[0] <= 0);
    }
}