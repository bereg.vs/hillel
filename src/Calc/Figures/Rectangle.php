<?php

namespace Nemo\Hillel\Calc\Figures;

class Rectangle extends Polygon
{
    protected static string $figureName = "Прямоугольник";

    protected int $amountOfParameters = 2;

    public function getSquare(): int|float
    {
        return $this->shapes[0] * $this->shapes[1];
    }

    public function getPerimeter(): int|float
    {
        return $this->shapes[0] * 2 + $this->shapes[1] * 2;
    }
}