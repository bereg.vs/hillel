<?php

namespace Nemo\Hillel\Calc\Figures;

class Triangle extends Polygon
{
    protected static string $figureName = "Треугольник";

    protected int $amountOfParameters = 3;

    public function getSquare(): float
    {
        $halfPerimeter = $this->getPerimeter()/2;

        $a = [];
        foreach($this->shapes as $shape)
            $a[] = $halfPerimeter - $shape;

        return round(sqrt($halfPerimeter * array_product($a)), 2);

    }

    public function getPerimeter(): int|float
    {
        return array_sum($this->shapes);
    }
}