<?php

namespace Nemo\Hillel\Calc\Figures;

use Nemo\Hillel\Calc\Interfaces\IAskInput;
use Nemo\Hillel\Calc\Interfaces\IHaveShape;

abstract class Figure implements IHaveShape, IAskInput
{
    protected array $shapes;
    protected static string $figureName;

    public static function getFigureName() : string
    {
        return static::$figureName;
    }

    public function getInfo() : void
    {
        echo "Фигура: " . $this->getFigureName() . PHP_EOL;
        echo "Периметр: " . $this->getPerimeter() . PHP_EOL;
        echo "Площадь: " . $this->getSquare() . PHP_EOL;
    }
}