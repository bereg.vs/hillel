<?php

namespace Nemo\Hillel\Calc\Figures;

abstract class Polygon extends Figure
{
    protected int $amountOfParameters = 1;

    public function askShapes(): void
    {
        for ($i = 0; $i<$this->amountOfParameters; $i++) {
            do {
                $this->shapes[$i] = readline('Укажи длинну стороны ' . $i+1 . ':' . PHP_EOL);
            } while (!is_numeric($this->shapes[$i]) || $this->shapes[$i] <= 0);
        }
    }
}