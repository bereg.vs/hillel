<?php

namespace Nemo\Hillel\Calc;

use Nemo\Hillel\Calc\Figures\Figure;

class Calc {

    /**
     * @param array $figures
     */
    public function __construct(public array $figures) {}

    /**
     * @param Figure $figure
     * @return void
     */
    public function registerFigure(Figure $figure) : void
    {
        $this->figures[] = $figure;
    }

    /**
     * @return array
     */
    public function getFigures() : array
    {
        return $this->figures;
    }

    /**
     * @param int $figureKey
     * @return void
     */
    public function run(int $figureKey): void
    {
        $figure = new $this->figures[$figureKey];

        $figure->askShapes();

        $figure->getInfo();
    }
}
