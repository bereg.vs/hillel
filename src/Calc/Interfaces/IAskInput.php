<?php

namespace Nemo\Hillel\Calc\Interfaces;

interface IAskInput
{
    public function askShapes() : void;
}