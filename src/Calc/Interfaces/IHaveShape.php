<?php

namespace Nemo\Hillel\Calc\Interfaces;

interface IHaveShape
{
    public function getSquare() : int|float;

    public function getPerimeter() : int|float;
}