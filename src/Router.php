<?php

namespace Nemo\Hillel;

class Router
{
    protected string $path;
    protected array $pathArray;

    protected string $arguments = '';
    protected array $argumentsArray = [];

    public function __construct(protected string $request)
    {
        $urlData = explode('?', $this->request);

        $this->path = $urlData[0];
        $this->arguments = $urlData[1] ?? '';

        $this->pathArray = array_values(array_filter(explode('/', $this->path)));
        parse_str($this->arguments, $this->argumentsArray);
    }

    public function getController(): string
    {
        $controllerName = __NAMESPACE__ . '\Controllers\\' . ucwords($this->pathArray[0]) . 'Controller';

        if (class_exists($controllerName)) {
            return $controllerName;
        }

        throw new \InvalidArgumentException('404');
    }

    public function getMethod(): string
    {
        if(!empty($this->pathArray[1]))
            $methodName = ucwords($this->pathArray[1]).'Action';
        else
            $methodName = 'indexAction';

        if (method_exists($this->getController(), $methodName)) {
            return $methodName;
        }

        throw new \InvalidArgumentException('404');
    }

    public function getArguments(): array
    {
        return $this->argumentsArray;
    }
}