<?php

namespace Nemo\Hillel\Controllers;

use Nemo\Hillel\LinkShortener\UrlCodeConverter;

class ShortenerController extends Controller
{
    public function __construct(protected UrlCodeConverter $converter)
    {
    }

    public function indexAction()
    {
        echo '/shortener/encode?url=<br>
        /shortener/decode?url=';
    }

    public function encodeAction($url)
    {
        echo $this->converter->encode($url);
    }

    public function decodeAction($url)
    {
        echo $this->converter->decode($url);
    }
}