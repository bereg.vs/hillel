CREATE TABLE `app`.`url_codes` (
   `id` INT NOT NULL AUTO_INCREMENT,
   `code` VARCHAR(255) NULL,
   `url` VARCHAR(255) NULL,
   PRIMARY KEY (`id`));
