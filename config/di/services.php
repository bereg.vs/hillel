<?php

use GuzzleHttp\ClientInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Nemo\Hillel\LinkShortener\Interfaces\ICodeRepository;
use Nemo\Hillel\LinkShortener\Interfaces\IUrlValidator;
use Nemo\Hillel\LinkShortener\Models\UrlCodeModel;
use Nemo\Hillel\LinkShortener\Repositories\FileRepository;
use Nemo\Hillel\LinkShortener\Repositories\MysqlRepository;
use Nemo\Hillel\LinkShortener\UrlValidator;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Illuminate\Database\Capsule\Manager as Capsule;

return [
    IUrlValidator::class => DI\autowire(UrlValidator::class),
    ClientInterface::class => DI\create(GuzzleHttp\Client::class),
    ICodeRepository::class => DI\get(FileRepository::class),

    FileRepository::class => function (ContainerInterface $c) {
        return new FileRepository($c->get('db_path'));
    },
    MysqlRepository::class => function (ContainerInterface $c) {
        $capsule = new Capsule;
        $capsule->addConnection($c->get('mysql'));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        return new MysqlRepository(UrlCodeModel::class);
    },
    LoggerInterface::class => function (ContainerInterface $c) {
        $logger = new Logger('shortener');

        $logger->pushHandler(new StreamHandler($c->get('logs_path') . '/warning.log', Level::Warning));
        $logger->pushHandler(new StreamHandler($c->get('logs_path') . '/info.log', Level::Info));

        return $logger;
    }
];