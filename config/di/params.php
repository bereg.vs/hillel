<?php

return [
    'db_path' => __DIR__.'/../../storage/db',
    'logs_path' =>  __DIR__.'/../../storage/logs',
    'mysql' => [
        'driver' => 'mysql',
        'host' => 'db',
        'database' => 'app',
        'username' => 'root',
        'password' => 'secret',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
    ]
];